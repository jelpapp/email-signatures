# Jelp Email Signatures

Repositorio donde se guardan las versiones de las firmas en HTML+CSS que posteriormente seran procesadas por MergeSignature para poder crear el archivo final y distribuirlo a cada usuario.

![Imagen Gustavo](/Firmas%20Jelp/Screen%20Shot%202018-10-29%20at%2011.07.35%20AM.png)

## Pasos para poder editar la firma

1. Clonar el presente repositorio
2. Importar los archivos en la platafomra Merge Signature
3. Realizar ajustes necesarios.
4. Copiar y pegar los archivos de regreso al repositorio.
5. Hacer Commit y push.
6. Despues de pruebas y autorizacion, se puede redistribuir.

## Pasos para distribuir la firma

En Merge Signature: 

1. Abrir una cuenta de Mergee Signature
2. Generar una nueva Firma
3. Copiar el codigo HTML que se tiene en el repositorio. Ver [Archivo HTML](Firmas%20Jelp/html.html) y pegarlo en Merge Signature
4. Lo imsmo con el codigo CSS que se tiene en el repositorio. Ver [Archivo CSS](Firmas%20Jelp/css.css)
5. Tener a la mano un listado de personas con los siguientes campos: [name, email, ext, image, position, phone, extension], Ejemplo: [Archivo CSV](Firmas%20Jelp/csv.csv)
6. Importar CSV a la plataforma de la misma manera que los archivos anteriores.
7. Generar las firmas.
8. Descargar cada una de las firmas (por usuario).
9. Enviar instrucciones a cada usuario para que pueda importarla. Basicamente tiene que abrir el archivo .html que le genera la plataforma con su nombre, seleccionar, copiar y pegar en las configuraciones de GSuite.

## TODO

### Operacional

* Generar instrucciones para poder recibir las firmas e implementarlas en Google Suite.
* Referencia: https://support.google.com/mail/answer/8395

### TODO tecnico
* Generar script para integrar CSS en HTML
* Generar API para enviarlo a GSuite Gmail API. Documentacion: [Aliases and Signatures](https://developers.google.com/gmail/api/guides/alias_and_signature_settings#managing_signatures)

## Autores

* Josue Basurto